#ifndef COMMON_HPP_
#define COMMON_HPP_

#include <vector>
#include <string>
#include <opencv2/core/core.hpp>
#include <dlib/image_processing.h>

// Global variables
static size_t landmark_n = 68;


namespace vh {

class Config {
public:
    static inline Config& GetInstance() {
        static Config c;
        return c;
    }

public:
    int stages_n;
    int tree_n;
    int tree_depth;
    bool flip;
    bool rotation;
    bool noise;
    bool scaling;
    bool blur;

    std::string dataset;
    std::string saved_file_name;
    std::string model_file_name;
    std::string test_input_filename;
    std::string train_input_filename;
    std::string prepare_input_filename;
    int partialData;
    unsigned int landmark_n;
    int initShape_n;
    std::vector<int> feats_m;
    std::vector<double> radius_m;
    double bagging_overlap;
    int eval_goodness;
    int start_models;
    int number_random_faces;
    std::vector<int> pupils[2];
    float threshold;
    int nbr_of_cores;

private:
    Config();
    ~Config() {}
    Config(const Config &other);
    Config &operator=(const Config &other);
};

class FaceBox {
public:
    FaceBox();
    ~FaceBox();
    FaceBox(double x, double y, double w, double h);

public:
    cv::Mat Project(const cv::Mat &shape) const;
    cv::Mat ReProject(const cv::Mat &shape) const;


public:
    double x, y;
    double x_center, y_center;
    double x_scale, y_scale;
    double width, height;
};

// General
double CLOCK();

// Measures - Metriccs
std::vector<std::vector<double> > get_interocular_distances_ert ( const std::vector<std::vector<dlib::full_object_detection> >& objects);
double get_mean_error(std::vector<cv::Mat> &gt_shapes, std::vector<cv::Mat> &predicted_shapes);
double get_success_rate(std::vector<cv::Mat> &gt_shapes, std::vector<cv::Mat> &predicted_shapes, float threshold);
// Benchmarks
double jitterErrorMeasurement(std::vector<cv::Mat> &shapes);

// Parsing databases files and read databases.
void loadDatasetJittering(std::string &txt, std::vector<cv::Mat> &imgs, std::vector<cv::Mat> &imgs_filtered, std::vector<cv::Mat> &gt_shapes, std::vector<FaceBox> &bboxes, bool crop);
void loadDataset(std::string &txt, std::vector<cv::Mat> &imgs, std::vector<cv::Mat> &imgs_filtered, std::vector<cv::Mat> &gt_shapes, std::vector<FaceBox> &bboxes, bool crop);


}

#endif // COMMON_HPP_
