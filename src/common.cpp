#include <cmath>
#include <ctime>
#include <cstdio>
#include <cassert>
#include <cstdarg>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "include/common.hpp"

using namespace cv;
using namespace std;

namespace vh {

FaceBox::FaceBox() {}
FaceBox::~FaceBox() {}
FaceBox::FaceBox(double x, double y, double w, double h) {
    this->x = x; this->y = y;
    this->width = w; this->height = h;
    this->x_center = x + w / 2.;
    this->y_center = y + h / 2.;
    this->x_scale = w / 2.;
    this->y_scale = h / 2.;
}


double CLOCK()
{
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC,  &t);
    return (t.tv_sec * 1000)+(t.tv_nsec*1e-6);
}


double interocular_distance ( const dlib::full_object_detection& det)
{
    dlib::vector<double,2> l, r;
    double cnt = 0;
    // Find the center of the left eye by averaging the points around
    // the eye.
    for (unsigned long i = 36; i <= 41; ++i)
    {
        l += det.part(i);
        ++cnt;
    }
    l /= cnt;

    // Find the center of the right eye by averaging the points around
    // the eye.
    cnt = 0;
    for (unsigned long i = 42; i <= 47; ++i)
    {
        r += det.part(i);
        ++cnt;
    }
    r /= cnt;

    // Now return the distance between the centers of the eyes
    return length(l-r);
}

std::vector<std::vector<double> > get_interocular_distances_ert ( const std::vector<std::vector<dlib::full_object_detection> >& objects)
{
    std::vector<std::vector<double> > temp(objects.size());
    for (unsigned long i = 0; i < objects.size(); ++i)
    {
        for (unsigned long j = 0; j < objects[i].size(); ++j)
        {
            temp[i].push_back(interocular_distance(objects[i][j]));
        }
    }
    return temp;
}




Config::Config() {


    // LBF MODELS PARAMETERS
    dataset = "../data";
    test_input_filename = "/common_subset_test.txt";
    train_input_filename = "/base_trainset.txt";
    saved_file_name = "../models/LBF565_GTX.model";
    model_file_name = "../models/LBF565_GTX.model";

    stages_n = 5;
    tree_n = 6;
    tree_depth = 5;
    initShape_n = 5;
    bagging_overlap = 0.4;
    flip = true;
    rotation = false;
    noise = false;
    scaling = false;
    blur = false;

    int feats_m[] = { 400, 400, 400, 400, 400, 400, 30, 30, 30, 30 };
    double radius_m[] = { 0.32, 0.25, 0.18, 0.15, 0.10, 0.10, 0.08, 0.6, 1.0, 0.8 };


    eval_goodness = 0;
    start_models = 31;
    number_random_faces = 0;
    landmark_n = 68;
    partialData = 0;
    threshold = 0.08f;
    nbr_of_cores = 8;


    if (landmark_n == 21)
    {
        int pupils[][1] = { {7 }, { 10 } };
        for (int i = 0; i < 1; i++) {
            this->pupils[0].push_back(pupils[0][i]);
            this->pupils[1].push_back(pupils[1][i]);
        }
    }
    else if (landmark_n == 68)
    {
        int pupils[][6] = { { 36, 37, 38, 39, 40, 41 }, { 42, 43, 44, 45, 46, 47 } };
        for (int i = 0; i < 6; i++) {
            this->pupils[0].push_back(pupils[0][i]);
            this->pupils[1].push_back(pupils[1][i]);
        }
    }



    for (int i = 0; i < 10; i++) {
        this->feats_m.push_back(feats_m[i]);
        this->radius_m.push_back(radius_m[i]);
    }
}


// Parse dataset text files without GT landmarks
void loadDatasetJittering(std::string &txt, std::vector<cv::Mat> &imgs, std::vector<cv::Mat> &imgs_filtered, std::vector<cv::Mat> &gt_shapes, std::vector<FaceBox> &bboxes, bool crop)
{
    std::cout << "Processing dataset file: " << txt << std::endl;
    Config &config = Config::GetInstance();
    FILE *fd = fopen(txt.c_str(), "r");
    assert(fd);

    unsigned int N;
    crop = false;
    unsigned int landmark_n = config.landmark_n;

    fscanf(fd, "%d", &N);
    std::cout << "    - Number of images in dataset: " << N << std::endl;

    imgs.resize(N);
    imgs_filtered.resize(N);
    gt_shapes.resize(N);

    bboxes.resize(N);
    char img_path[256];
    double bbox[4];
    vector<double> x(landmark_n), y(landmark_n);

    for (size_t i = 0; i < N; i++)
    {
        fscanf(fd, "%s", img_path);
        for (int j = 0; j < 4; j++) {
            fscanf(fd, "%lf", &bbox[j]);
        }

        Mat img_or = cv::imread(img_path, cv::IMREAD_COLOR);
        cv::Mat img;
        cv::cvtColor(img_or, img, cv::COLOR_BGR2GRAY);

        std::cout << "Image "<< i <<  " loaded - " << img_path << std::endl;
        std::cout.flush();

        // crop img
        double x_, y_, w_, h_;

        if (crop == false)
        {
            x_ = 0;
            y_ = 0;

            w_ = img.cols - 1;
            h_ = img.rows - 1;
        }
        else
        {
            double x_min, y_min, x_max, y_max;
            x_min = *min_element(x.begin(), x.end());
            x_max = *max_element(x.begin(), x.end());
            y_min = *min_element(y.begin(), y.end());
            y_max = *max_element(y.begin(), y.end());
            x_min = max(0., x_min - bbox[2] / 1.5);
            x_max = min(img.cols - 1., x_max + bbox[2] / 1.5);
            y_min = max(0., y_min - bbox[3] / 1.5);
            y_max = min(img.rows - 1., y_max + bbox[3] / 1.5);
            x_ = x_min; y_ = y_min;
            w_ = x_max - x_min; h_ = y_max - y_min;
        }


        bboxes[i] = vh::FaceBox(bbox[0] - x_, bbox[1] - y_, bbox[2], bbox[3]);

        cv::Rect roi(x_, y_, w_, h_);
        imgs[i] = img(roi).clone();
        imgs_filtered[i] = img_or(roi).clone();

    }
    fclose(fd);
}


void loadDataset(std::string &txt, std::vector<cv::Mat> &imgs, std::vector<cv::Mat> &imgs_filtered, std::vector<cv::Mat> &gt_shapes, std::vector<FaceBox> &bboxes, bool crop)
{
    std::cout << "Processing dataset file: " << txt << std::endl;
    Config &config = Config::GetInstance();
    FILE *fd = fopen(txt.c_str(), "r");
    assert(fd);

    unsigned int N;
    crop = false;
    unsigned int landmark_n = config.landmark_n;

    fscanf(fd, "%d", &N);
    std::cout << "    - Number of images in dataset: " << N << std::endl;

    imgs.resize(N);
    imgs_filtered.resize(N);
    gt_shapes.resize(N);

    bboxes.resize(N);
    char img_path[256];
    double bbox[4];
    vector<double> x(landmark_n), y(landmark_n);

    for (size_t i = 0; i < N; i++)
    {
        fscanf(fd, "%s", img_path);
        for (int j = 0; j < 4; j++) {
            fscanf(fd, "%lf", &bbox[j]);
        }
        for (size_t j = 0; j < landmark_n; j++) {
            fscanf(fd, "%lf%lf", &x[j], &y[j]);
        }

        cv::Mat img_or = cv::imread(img_path, cv::IMREAD_COLOR);
        cv::Mat img;
        cv::cvtColor(img_or, img, cv::COLOR_BGR2GRAY);

        std::cout << "Image "<< i <<  " loaded - " << img_path << std::endl;
        std::cout.flush();


        double x_, y_, w_, h_;
        if (crop == false)
        {
            x_ = 0;
            y_ = 0;
            w_ = img.cols - 1;
            h_ = img.rows - 1;
        }
        else
        {
            double x_min, y_min, x_max, y_max;
            x_min = *min_element(x.begin(), x.end());
            x_max = *max_element(x.begin(), x.end());
            y_min = *min_element(y.begin(), y.end());
            y_max = *max_element(y.begin(), y.end());
            x_min = max(0., x_min - bbox[2] / 1.5);
            x_max = min(img.cols - 1., x_max + bbox[2] / 1.5);
            y_min = max(0., y_min - bbox[3] / 1.5);
            y_max = min(img.rows - 1., y_max + bbox[3] / 1.5);
            x_ = x_min; y_ = y_min;
            w_ = x_max - x_min; h_ = y_max - y_min;
        }


        bboxes[i] = vh::FaceBox(bbox[0] - x_, bbox[1] - y_, bbox[2], bbox[3]);
        gt_shapes[i] = cv::Mat::zeros(static_cast<int>(landmark_n), 2, CV_64FC1);
        for (size_t j = 0; j < landmark_n; j++) {
            gt_shapes[i].at<double>(j, 0) = x[j] - x_;
            gt_shapes[i].at<double>(j, 1) = y[j] - y_;
        }
        cv::Rect roi(x_, y_, w_, h_);
        imgs[i] = img(roi).clone();
        imgs_filtered[i] = img_or(roi).clone();

    }
    fclose(fd);
}



double jitterErrorMeasurement(std::vector<cv::Mat> &shapes)
{
    size_t N = shapes.size();
    Config &config = Config::GetInstance();
    unsigned int landmark_n = config.landmark_n;
    vector<int> &left = config.pupils[0];
    vector<int> &right = config.pupils[1];

    double e = 0;
    int control = 0;
    // every train data
    for (size_t i = 1; i < N; i++) {
        const Mat_<double> &previous_shape = static_cast<Mat_<double>>(shapes[i-1]);
        const Mat_<double> &current_shape = static_cast<Mat_<double>>(shapes[i]);
        double x1, y1, x2, y2;
        x1 = x2 = y1 = y2 = 0;

        for (unsigned long j = 0; j < left.size(); j++) {
            x1 += previous_shape(left[j], 0);
            y1 += previous_shape(left[j], 1);
        }
        for (unsigned long j = 0; j < right.size(); j++) {
            x2 += previous_shape(right[j], 0);
            y2 += previous_shape(right[j], 1);
        }

        x1 /= left.size(); y1 /= left.size();
        x2 /= right.size(); y2 /= right.size();
        double e_ = 0;
        double pupils_distance = 0;
        if (x1 != -999.0 && x2 != -999.0)
        {
            // Computing difference of frame i
            pupils_distance = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

            // every landmark
            int contributors = 0;
            e_ = 0;
            for (int i = 0; i < landmark_n; i++)
            {
                contributors++;
                e_ += cv::norm(current_shape.row(i) - previous_shape.row(i));
            }

            control++;
        }
        e += e_;
    }

    std::cout << "    - Interframe total pixel movement due to Jitter: " << e << std::endl;
    e /= control;
    std::cout << "    - Interframe mean pixel movement due to Jitter: " <<  e << std::endl;
    return e;
}



double get_mean_error(std::vector<cv::Mat> &gt_shapes, std::vector<cv::Mat> &predicted_shapes)
{
    size_t N = gt_shapes.size();
    Config &config = Config::GetInstance();
    size_t landmark_n = config.landmark_n;
    std::vector<int> &left = config.pupils[0];
    std::vector<int> &right = config.pupils[1];

    double e = 0;

    // every train data
    for (size_t i = 0; i < N; i++)
    {
        const cv::Mat_<double> &gt_shape = static_cast<cv::Mat_<double>>(gt_shapes[i]);
        const cv::Mat_<double> &current_shape = static_cast<cv::Mat_<double>>(predicted_shapes[i]);
        double x1, y1, x2, y2;
        x1 = x2 = y1 = y2 = 0;
        for (int j = 0; j < left.size(); j++) {
            x1 += gt_shape(left[j], 0);
            y1 += gt_shape(left[j], 1);
        }
        for (int j = 0; j < right.size(); j++) {
            x2 += gt_shape(right[j], 0);
            y2 += gt_shape(right[j], 1);
        }
        x1 /= left.size(); y1 /= left.size();
        x2 /= right.size(); y2 /= right.size();

        double pupils_distance = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

        // every landmark
        double e_ = 0;
        for (size_t i = 0; i < landmark_n; i++) {
            e_ += cv::norm(gt_shape.row(i) - current_shape.row(i));
        }
        e += e_ / pupils_distance;

    }
    e /= N*landmark_n;
    return e;
}



double get_success_rate(vector<Mat> &gt_shapes, vector<Mat> &predicted_shapes, float threshold)
{
    size_t N = gt_shapes.size();
    Config &config = Config::GetInstance();
    size_t landmark_n = config.landmark_n;
    vector<int> &left = config.pupils[0];
    vector<int> &right = config.pupils[1];

    double e = 0;
    int control = 0;

    // every train data
    for (size_t i = 0; i < N; i++)
    {
        const cv::Mat_<double> &gt_shape = static_cast<cv::Mat_<double>>(gt_shapes[i]);
        const cv::Mat_<double> &current_shape = static_cast<cv::Mat_<double>>(predicted_shapes[i]);
        double x1, y1, x2, y2;
        x1 = x2 = y1 = y2 = 0;
        for (size_t j = 0; j < left.size(); j++) {
            x1 += gt_shape(left[j], 0);
            y1 += gt_shape(left[j], 1);
        }
        for (size_t j = 0; j < right.size(); j++) {
            x2 += gt_shape(right[j], 0);
            y2 += gt_shape(right[j], 1);
        }
        x1 /= left.size(); y1 /= left.size();
        x2 /= right.size(); y2 /= right.size();
        if (x1 != -999.0 && x2 != -999.0)
        {
            double pupils_distance = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

            // every landmark
            int contributors = 0;
            double e_ = 0;
            for (size_t i = 0; i < landmark_n; i++) {
                if (gt_shape(i, 0) != -999.0)
                {
                    contributors++;
                    e_ += cv::norm(gt_shape.row(i) - current_shape.row(i));
                }
            }

            if ((e_ / pupils_distance) < (threshold*contributors))
                e++;

            control++;
        }
    }

    e /= control;
    return e;
}


} // namespace vh
