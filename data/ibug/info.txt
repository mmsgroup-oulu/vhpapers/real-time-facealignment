Folder to place the ibug data.

Notes:
    - Download databases from: https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
    - We already provide the annotations ready to read from the project in .txt and .xml formats.

