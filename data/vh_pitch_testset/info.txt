Folder to place the pitch related data.

Notes:
    - Download public databases from: https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
    - Download private datasets from: https://drive.google.com/drive/folders/1o5qMIVMQcnvlufn1vLPi9WF7-h0id41l?usp=sharing
    - We already provide the annotations ready to read from the project in .txt and .xml formats.

